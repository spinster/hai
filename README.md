

```python
import csv
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
%matplotlib inline
```


```python
#read csv
hai_csv = pd.read_csv('csvs/Healthcare_Associated_Infections_-_Hospital.csv')
```


```python
#convert csv into dataframe
hai_df = pd.DataFrame(hai_csv)
```


```python
hai_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Provider ID</th>
      <th>Hospital Name</th>
      <th>Address</th>
      <th>City</th>
      <th>State</th>
      <th>ZIP Code</th>
      <th>County Name</th>
      <th>Phone Number</th>
      <th>Measure Name</th>
      <th>Measure ID</th>
      <th>Compared to National</th>
      <th>Score</th>
      <th>Footnote</th>
      <th>Measure Start Date</th>
      <th>Measure End Date</th>
      <th>Location</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>190046</td>
      <td>TOURO INFIRMARY</td>
      <td>1401 FOUCHER STREET</td>
      <td>NEW ORLEANS</td>
      <td>LA</td>
      <td>70115</td>
      <td>ORLEANS</td>
      <td>5048978247</td>
      <td>SSI: Abdominal, Number of Procedures</td>
      <td>HAI_4_DOPC_DAYS</td>
      <td>No Different than National Benchmark</td>
      <td>158</td>
      <td>NaN</td>
      <td>04/01/2016</td>
      <td>03/31/2017</td>
      <td>1401 FOUCHER STREET\nNEW ORLEANS, LA\n(29.9253...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>190041</td>
      <td>CHRISTUS HEALTH SHREVEPORT - BOSSIER</td>
      <td>1453 E BERT KOUNS INDUSTRIAL DRIVE</td>
      <td>SHREVEPORT</td>
      <td>LA</td>
      <td>71105</td>
      <td>CADDO</td>
      <td>3186815000</td>
      <td>SSI: Colon Predicted Cases</td>
      <td>HAI_3_ELIGCASES</td>
      <td>No Different than National Benchmark</td>
      <td>3.819</td>
      <td>NaN</td>
      <td>04/01/2016</td>
      <td>03/31/2017</td>
      <td>1453 E BERT KOUNS INDUSTRIAL DRIVE\nSHREVEPORT...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>190040</td>
      <td>SLIDELL MEMORIAL HOSPITAL</td>
      <td>1001 GAUSE BLVD</td>
      <td>SLIDELL</td>
      <td>LA</td>
      <td>70458</td>
      <td>SAINT TAMMANY</td>
      <td>9856432200</td>
      <td>C.diff Lower Confidence Limit</td>
      <td>HAI_6_CI_LOWER</td>
      <td>No Different than National Benchmark</td>
      <td>0.252</td>
      <td>NaN</td>
      <td>04/01/2016</td>
      <td>03/31/2017</td>
      <td>1001 GAUSE BLVD\nSLIDELL, LA\n(30.284651, -89....</td>
    </tr>
    <tr>
      <th>3</th>
      <td>190054</td>
      <td>IBERIA GENERAL HOSPITAL AND MEDICAL CENTER</td>
      <td>2315 E MAIN STREET</td>
      <td>NEW IBERIA</td>
      <td>LA</td>
      <td>70562</td>
      <td>IBERIA</td>
      <td>3373640441</td>
      <td>Surgical site infections (SSI) from abdominal ...</td>
      <td>HAI_4_SIR</td>
      <td>Not Available</td>
      <td>Not Available</td>
      <td>13 - Results cannot be calculated for this rep...</td>
      <td>04/01/2016</td>
      <td>03/31/2017</td>
      <td>2315 E MAIN STREET\nNEW IBERIA, LA\n(29.989047...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>190027</td>
      <td>CHRISTUS ST PATRICK HOSPITAL</td>
      <td>524 DR MICHAEL DEBAKEY STREET</td>
      <td>LAKE CHARLES</td>
      <td>LA</td>
      <td>70601</td>
      <td>CALCASIEU</td>
      <td>3374362511</td>
      <td>SSI: Colon Predicted Cases</td>
      <td>HAI_3_ELIGCASES</td>
      <td>No Different than National Benchmark</td>
      <td>1.662</td>
      <td>NaN</td>
      <td>04/01/2016</td>
      <td>03/31/2017</td>
      <td>524 DR MICHAEL DEBAKEY STREET\nLAKE CHARLES, L...</td>
    </tr>
  </tbody>
</table>
</div>




```python
#unique hospitals
unique_hospitals = hai_df['Hospital Name'].unique()
len(unique_hospitals)
```




    4620




```python
#Drop unnecessary data
hai_clean_df = hai_df.drop(['Provider ID', 
             'Phone Number',
             'Measure ID',
             'Footnote',
             'Measure Start Date',
             'Measure End Date'],axis = 1)
```


```python
#Read in csv of hospital list with hospital demographics
hospitals_csv = pd.read_csv('csvs/hospitals_db.csv')
```


```python
#Create DF of hospital list
hospitals_df = pd.DataFrame(hospitals_csv)
```


```python
#Drop unnecessary data from the hospital DF
hospitals_clean_df = hospitals_df.drop(['OBJECTID','ID','ST_FIPS'], axis = 1)
```


```python
#Rename columns to match with the hospital column on which to merge DFs
hai_clean_df = hai_clean_df.rename(columns = {'Hospital Name': 'NAME',
                                'Address':'ADDRESS',
                                'City':'CITY',
                                'State':'STATE',
                                'ZIP Code':'ZIP',
                                'County Name':'COUNTY',
                                'Measure Name':'MEASURE',
                                'Compared to National':'COMPARED TO NATIONAL',
                                'Score':'SCORE',
                                'Location':'Location'})
```


```python
#Merging the list of hospitals with this list of infections
hai_hospital_merge_df =  pd.merge(hai_clean_df, hospitals_clean_df, how='left', on='NAME')
```


```python
#Removing redundant data
#Population == number of beds
hai_hospital_merge_df = hai_hospital_merge_df.drop(['ADDRESS_y', 'ZIP_y','CITY_y','STATE_y','COUNTY_y','TRAUMA', 'POPULATION'],axis = 1)
```


```python
#Renaming columns. Caps was a mistake
hai_hospital_merge_df = hai_hospital_merge_df.rename(columns = {'NAME':'Name',
                                                                'ADDRESS_x':'Address',
                                                                'CITY_x':'City',
                                                                'STATE_x':'State',
                                                                'ZIP_x':'ZIP',
                                                                'COUNTY_x':'County',
                                                                'MEASURE':'Measure',
                                                                'COMPARED TO NATIONAL':'Compared to National',
                                                                'SCORE':'Score',
                                                                'TYPE':'Type',
                                                                'STATUS':'Status',
                                                                'NAICS_DESC':'NAICS_DESC',
                                                                'OWNER':'Owner',
                                                                'BEDS':'Beds',
                                                                'DATECREATE':'Date Created',
                                                                'Location':'Location'})
```


```python
hai_hospital_merge_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Name</th>
      <th>Address</th>
      <th>City</th>
      <th>State</th>
      <th>ZIP</th>
      <th>County</th>
      <th>Measure</th>
      <th>Compared to National</th>
      <th>Score</th>
      <th>Location</th>
      <th>Type</th>
      <th>Status</th>
      <th>NAICS_DESC</th>
      <th>Owner</th>
      <th>Beds</th>
      <th>Date Created</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>TOURO INFIRMARY</td>
      <td>1401 FOUCHER STREET</td>
      <td>NEW ORLEANS</td>
      <td>LA</td>
      <td>70115</td>
      <td>ORLEANS</td>
      <td>SSI: Abdominal, Number of Procedures</td>
      <td>No Different than National Benchmark</td>
      <td>158</td>
      <td>1401 FOUCHER STREET\nNEW ORLEANS, LA\n(29.9253...</td>
      <td>GENERAL ACUTE CARE</td>
      <td>OPEN</td>
      <td>GENERAL MEDICAL AND SURGICAL HOSPITALS</td>
      <td>NON-PROFIT</td>
      <td>411.0</td>
      <td>2013-05-30T00:00:00.000Z</td>
    </tr>
    <tr>
      <th>1</th>
      <td>CHRISTUS HEALTH SHREVEPORT - BOSSIER</td>
      <td>1453 E BERT KOUNS INDUSTRIAL DRIVE</td>
      <td>SHREVEPORT</td>
      <td>LA</td>
      <td>71105</td>
      <td>CADDO</td>
      <td>SSI: Colon Predicted Cases</td>
      <td>No Different than National Benchmark</td>
      <td>3.819</td>
      <td>1453 E BERT KOUNS INDUSTRIAL DRIVE\nSHREVEPORT...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2</th>
      <td>SLIDELL MEMORIAL HOSPITAL</td>
      <td>1001 GAUSE BLVD</td>
      <td>SLIDELL</td>
      <td>LA</td>
      <td>70458</td>
      <td>SAINT TAMMANY</td>
      <td>C.diff Lower Confidence Limit</td>
      <td>No Different than National Benchmark</td>
      <td>0.252</td>
      <td>1001 GAUSE BLVD\nSLIDELL, LA\n(30.284651, -89....</td>
      <td>GENERAL ACUTE CARE</td>
      <td>OPEN</td>
      <td>GENERAL MEDICAL AND SURGICAL HOSPITALS</td>
      <td>GOVERNMENT - DISTRICT/AUTHORITY</td>
      <td>170.0</td>
      <td>2013-05-30T00:00:00.000Z</td>
    </tr>
    <tr>
      <th>3</th>
      <td>IBERIA GENERAL HOSPITAL AND MEDICAL CENTER</td>
      <td>2315 E MAIN STREET</td>
      <td>NEW IBERIA</td>
      <td>LA</td>
      <td>70562</td>
      <td>IBERIA</td>
      <td>Surgical site infections (SSI) from abdominal ...</td>
      <td>Not Available</td>
      <td>Not Available</td>
      <td>2315 E MAIN STREET\nNEW IBERIA, LA\n(29.989047...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>4</th>
      <td>CHRISTUS ST PATRICK HOSPITAL</td>
      <td>524 DR MICHAEL DEBAKEY STREET</td>
      <td>LAKE CHARLES</td>
      <td>LA</td>
      <td>70601</td>
      <td>CALCASIEU</td>
      <td>SSI: Colon Predicted Cases</td>
      <td>No Different than National Benchmark</td>
      <td>1.662</td>
      <td>524 DR MICHAEL DEBAKEY STREET\nLAKE CHARLES, L...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>




```python
#Remove any entry that does not have a national benchmark, owner, or number of beds not available. 
#The hospital list includes hospitals not on the HAI list
hai_hospitals_merge_df = hai_hospital_merge_df.loc[hai_hospital_merge_df['Compared to National'] != 'Not Available']
hai_hospitals_merge_df = hai_hospital_merge_df.loc[hai_hospital_merge_df['Owner'] != 'Not Available']
hai_hospitals_merge_df = hai_hospital_merge_df.loc[hai_hospital_merge_df['Beds'] != -999.0]

#Remove entries that do not have a type. Those with no type do not have enough data to analyze
hospitals_df = hai_hospitals_merge_df.dropna(how='all', subset=['Type'])
```


```python
hai_hospitals_merge_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Name</th>
      <th>Address</th>
      <th>City</th>
      <th>State</th>
      <th>ZIP</th>
      <th>County</th>
      <th>Measure</th>
      <th>Compared to National</th>
      <th>Score</th>
      <th>Location</th>
      <th>Type</th>
      <th>Status</th>
      <th>NAICS_DESC</th>
      <th>Owner</th>
      <th>Beds</th>
      <th>Date Created</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>TOURO INFIRMARY</td>
      <td>1401 FOUCHER STREET</td>
      <td>NEW ORLEANS</td>
      <td>LA</td>
      <td>70115</td>
      <td>ORLEANS</td>
      <td>SSI: Abdominal, Number of Procedures</td>
      <td>No Different than National Benchmark</td>
      <td>158</td>
      <td>1401 FOUCHER STREET\nNEW ORLEANS, LA\n(29.9253...</td>
      <td>GENERAL ACUTE CARE</td>
      <td>OPEN</td>
      <td>GENERAL MEDICAL AND SURGICAL HOSPITALS</td>
      <td>NON-PROFIT</td>
      <td>411.0</td>
      <td>2013-05-30T00:00:00.000Z</td>
    </tr>
    <tr>
      <th>1</th>
      <td>CHRISTUS HEALTH SHREVEPORT - BOSSIER</td>
      <td>1453 E BERT KOUNS INDUSTRIAL DRIVE</td>
      <td>SHREVEPORT</td>
      <td>LA</td>
      <td>71105</td>
      <td>CADDO</td>
      <td>SSI: Colon Predicted Cases</td>
      <td>No Different than National Benchmark</td>
      <td>3.819</td>
      <td>1453 E BERT KOUNS INDUSTRIAL DRIVE\nSHREVEPORT...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2</th>
      <td>SLIDELL MEMORIAL HOSPITAL</td>
      <td>1001 GAUSE BLVD</td>
      <td>SLIDELL</td>
      <td>LA</td>
      <td>70458</td>
      <td>SAINT TAMMANY</td>
      <td>C.diff Lower Confidence Limit</td>
      <td>No Different than National Benchmark</td>
      <td>0.252</td>
      <td>1001 GAUSE BLVD\nSLIDELL, LA\n(30.284651, -89....</td>
      <td>GENERAL ACUTE CARE</td>
      <td>OPEN</td>
      <td>GENERAL MEDICAL AND SURGICAL HOSPITALS</td>
      <td>GOVERNMENT - DISTRICT/AUTHORITY</td>
      <td>170.0</td>
      <td>2013-05-30T00:00:00.000Z</td>
    </tr>
    <tr>
      <th>3</th>
      <td>IBERIA GENERAL HOSPITAL AND MEDICAL CENTER</td>
      <td>2315 E MAIN STREET</td>
      <td>NEW IBERIA</td>
      <td>LA</td>
      <td>70562</td>
      <td>IBERIA</td>
      <td>Surgical site infections (SSI) from abdominal ...</td>
      <td>Not Available</td>
      <td>Not Available</td>
      <td>2315 E MAIN STREET\nNEW IBERIA, LA\n(29.989047...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>4</th>
      <td>CHRISTUS ST PATRICK HOSPITAL</td>
      <td>524 DR MICHAEL DEBAKEY STREET</td>
      <td>LAKE CHARLES</td>
      <td>LA</td>
      <td>70601</td>
      <td>CALCASIEU</td>
      <td>SSI: Colon Predicted Cases</td>
      <td>No Different than National Benchmark</td>
      <td>1.662</td>
      <td>524 DR MICHAEL DEBAKEY STREET\nLAKE CHARLES, L...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>




```python
#Combine small to mid sized hospitals into one owner
hospitals_df.loc[hospitals_df['Owner'] == 'GOVERNMENT - DISTRICT/AUTHORITY','Owner'] = 'REGIONAL GOVERNMENT'

hospitals_df.loc[hospitals_df['Owner'] == 'GOVERNMENT - LOCAL','Owner'] == 'REGIONAL GOVERNMENT'
```

    /home/spinster/anaconda3/envs/PythonData/lib/python3.6/site-packages/pandas/core/indexing.py:537: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: http://pandas.pydata.org/pandas-docs/stable/indexing.html#indexing-view-versus-copy
      self.obj[item] = s





    600       False
    1462      False
    1468      False
    1500      False
    1649      False
    1683      False
    1694      False
    1699      False
    1718      False
    1730      False
    1752      False
    1757      False
    1765      False
    1774      False
    1792      False
    1805      False
    1825      False
    1826      False
    1827      False
    1828      False
    1829      False
    1830      False
    1831      False
    1832      False
    1833      False
    1834      False
    1835      False
    1836      False
    1837      False
    1838      False
              ...  
    197404    False
    197414    False
    197419    False
    197420    False
    197428    False
    197434    False
    197444    False
    197446    False
    197452    False
    197471    False
    197482    False
    197490    False
    197495    False
    197498    False
    197505    False
    197513    False
    197522    False
    197524    False
    197532    False
    197540    False
    197543    False
    197552    False
    197559    False
    197567    False
    197587    False
    197596    False
    197609    False
    197618    False
    197631    False
    197638    False
    Name: Owner, Length: 15768, dtype: bool




```python
grouped_hospitals_df = hospitals_df.sort_values(by=['Name', 'Measure'], axis = 0)
```


```python
grouped_hospitals_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Name</th>
      <th>Address</th>
      <th>City</th>
      <th>State</th>
      <th>ZIP</th>
      <th>County</th>
      <th>Measure</th>
      <th>Compared to National</th>
      <th>Score</th>
      <th>Location</th>
      <th>Type</th>
      <th>Status</th>
      <th>NAICS_DESC</th>
      <th>Owner</th>
      <th>Beds</th>
      <th>Date Created</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>67104</th>
      <td>ABBEVILLE AREA MEDICAL CENTER</td>
      <td>420 THOMSON CIRCLE</td>
      <td>ABBEVILLE</td>
      <td>SC</td>
      <td>29620</td>
      <td>ABBEVILLE</td>
      <td>C.diff Lower Confidence Limit</td>
      <td>No Different than National Benchmark</td>
      <td>Not Available</td>
      <td>420 THOMSON CIRCLE\nABBEVILLE, SC\n(34.154293,...</td>
      <td>GENERAL ACUTE CARE</td>
      <td>OPEN</td>
      <td>GENERAL MEDICAL AND SURGICAL HOSPITALS</td>
      <td>GOVERNMENT - LOCAL</td>
      <td>25.0</td>
      <td>2013-03-08T00:00:00.000Z</td>
    </tr>
    <tr>
      <th>67133</th>
      <td>ABBEVILLE AREA MEDICAL CENTER</td>
      <td>420 THOMSON CIRCLE</td>
      <td>ABBEVILLE</td>
      <td>SC</td>
      <td>29620</td>
      <td>ABBEVILLE</td>
      <td>C.diff Observed Cases</td>
      <td>No Different than National Benchmark</td>
      <td>0</td>
      <td>420 THOMSON CIRCLE\nABBEVILLE, SC\n(34.154293,...</td>
      <td>GENERAL ACUTE CARE</td>
      <td>OPEN</td>
      <td>GENERAL MEDICAL AND SURGICAL HOSPITALS</td>
      <td>GOVERNMENT - LOCAL</td>
      <td>25.0</td>
      <td>2013-03-08T00:00:00.000Z</td>
    </tr>
    <tr>
      <th>66831</th>
      <td>ABBEVILLE AREA MEDICAL CENTER</td>
      <td>420 THOMSON CIRCLE</td>
      <td>ABBEVILLE</td>
      <td>SC</td>
      <td>29620</td>
      <td>ABBEVILLE</td>
      <td>C.diff Patient Days</td>
      <td>No Different than National Benchmark</td>
      <td>3797</td>
      <td>420 THOMSON CIRCLE\nABBEVILLE, SC\n(34.154293,...</td>
      <td>GENERAL ACUTE CARE</td>
      <td>OPEN</td>
      <td>GENERAL MEDICAL AND SURGICAL HOSPITALS</td>
      <td>GOVERNMENT - LOCAL</td>
      <td>25.0</td>
      <td>2013-03-08T00:00:00.000Z</td>
    </tr>
    <tr>
      <th>67501</th>
      <td>ABBEVILLE AREA MEDICAL CENTER</td>
      <td>420 THOMSON CIRCLE</td>
      <td>ABBEVILLE</td>
      <td>SC</td>
      <td>29620</td>
      <td>ABBEVILLE</td>
      <td>C.diff Predicted Cases</td>
      <td>No Different than National Benchmark</td>
      <td>1.039</td>
      <td>420 THOMSON CIRCLE\nABBEVILLE, SC\n(34.154293,...</td>
      <td>GENERAL ACUTE CARE</td>
      <td>OPEN</td>
      <td>GENERAL MEDICAL AND SURGICAL HOSPITALS</td>
      <td>GOVERNMENT - LOCAL</td>
      <td>25.0</td>
      <td>2013-03-08T00:00:00.000Z</td>
    </tr>
    <tr>
      <th>66673</th>
      <td>ABBEVILLE AREA MEDICAL CENTER</td>
      <td>420 THOMSON CIRCLE</td>
      <td>ABBEVILLE</td>
      <td>SC</td>
      <td>29620</td>
      <td>ABBEVILLE</td>
      <td>C.diff Upper Confidence Limit</td>
      <td>No Different than National Benchmark</td>
      <td>2.883</td>
      <td>420 THOMSON CIRCLE\nABBEVILLE, SC\n(34.154293,...</td>
      <td>GENERAL ACUTE CARE</td>
      <td>OPEN</td>
      <td>GENERAL MEDICAL AND SURGICAL HOSPITALS</td>
      <td>GOVERNMENT - LOCAL</td>
      <td>25.0</td>
      <td>2013-03-08T00:00:00.000Z</td>
    </tr>
  </tbody>
</table>
</div>




```python
#have to group by provider id because of duplicate names
#GPL 3.0/HT: This cell contributed by Matthew Young
scores = grouped_hospitals_df.groupby(['Name','Measure']).first()['Score'].unstack(level=-1)
scores = scores.reset_index()
hosps = grouped_hospitals_df[['Name', 'Beds', 'Type', 'Owner','City','State','ZIP']].drop_duplicates()
hosps_scores = pd.merge(scores,hosps,on='Name')
#hosps_scores
```


```python
hosps_scores.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Name</th>
      <th>C.diff Lower Confidence Limit</th>
      <th>C.diff Observed Cases</th>
      <th>C.diff Patient Days</th>
      <th>C.diff Predicted Cases</th>
      <th>C.diff Upper Confidence Limit</th>
      <th>CAUTI: Lower Confidence Limit</th>
      <th>CAUTI: Number of Urinary Catheter Days</th>
      <th>CAUTI: Observed Cases</th>
      <th>CAUTI: Predicted Cases</th>
      <th>...</th>
      <th>SSI: Colon Upper Confidence Limit</th>
      <th>SSI: Colon, Number of Procedures</th>
      <th>Surgical site infections (SSI) from abdominal hysterectomy</th>
      <th>Surgical site infections (SSI) from colon surgery</th>
      <th>Beds</th>
      <th>Type</th>
      <th>Owner</th>
      <th>City</th>
      <th>State</th>
      <th>ZIP</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>ABBEVILLE AREA MEDICAL CENTER</td>
      <td>Not Available</td>
      <td>0</td>
      <td>3797</td>
      <td>1.039</td>
      <td>2.883</td>
      <td>Not Available</td>
      <td>566</td>
      <td>0</td>
      <td>0.370</td>
      <td>...</td>
      <td>Not Available</td>
      <td>11</td>
      <td>Not Available</td>
      <td>Not Available</td>
      <td>25.0</td>
      <td>GENERAL ACUTE CARE</td>
      <td>GOVERNMENT - LOCAL</td>
      <td>ABBEVILLE</td>
      <td>SC</td>
      <td>29620</td>
    </tr>
    <tr>
      <th>1</th>
      <td>ABBEVILLE GENERAL HOSPITAL</td>
      <td>0.822</td>
      <td>5</td>
      <td>5392</td>
      <td>2.228</td>
      <td>4.974</td>
      <td>0.044</td>
      <td>2183</td>
      <td>1</td>
      <td>1.126</td>
      <td>...</td>
      <td>Not Available</td>
      <td>17</td>
      <td>Not Available</td>
      <td>Not Available</td>
      <td>60.0</td>
      <td>GENERAL ACUTE CARE</td>
      <td>REGIONAL GOVERNMENT</td>
      <td>ABBEVILLE</td>
      <td>LA</td>
      <td>70510</td>
    </tr>
    <tr>
      <th>2</th>
      <td>ABBOTT NORTHWESTERN HOSPITAL</td>
      <td>0.495</td>
      <td>79</td>
      <td>166574</td>
      <td>127.312</td>
      <td>0.769</td>
      <td>0.697</td>
      <td>19667</td>
      <td>26</td>
      <td>24.887</td>
      <td>...</td>
      <td>2.274</td>
      <td>342</td>
      <td>1.767</td>
      <td>1.308</td>
      <td>952.0</td>
      <td>GENERAL ACUTE CARE</td>
      <td>NON-PROFIT</td>
      <td>MINNEAPOLIS</td>
      <td>MN</td>
      <td>55407</td>
    </tr>
    <tr>
      <th>3</th>
      <td>ABILENE REGIONAL MEDICAL CENTER</td>
      <td>0.281</td>
      <td>9</td>
      <td>21895</td>
      <td>15.599</td>
      <td>1.059</td>
      <td>0.017</td>
      <td>3668</td>
      <td>1</td>
      <td>3.030</td>
      <td>...</td>
      <td>2.484</td>
      <td>48</td>
      <td>Not Available</td>
      <td>0.000</td>
      <td>231.0</td>
      <td>GENERAL ACUTE CARE</td>
      <td>PROPRIETARY</td>
      <td>ABILENE</td>
      <td>TX</td>
      <td>79606</td>
    </tr>
    <tr>
      <th>4</th>
      <td>ABINGTON MEMORIAL HOSPITAL</td>
      <td>0.389</td>
      <td>52</td>
      <td>133135</td>
      <td>100.859</td>
      <td>0.671</td>
      <td>1.134</td>
      <td>15988</td>
      <td>32</td>
      <td>19.639</td>
      <td>...</td>
      <td>2.145</td>
      <td>362</td>
      <td>0.377</td>
      <td>1.234</td>
      <td>665.0</td>
      <td>GENERAL ACUTE CARE</td>
      <td>NON-PROFIT</td>
      <td>ABINGTON</td>
      <td>PA</td>
      <td>19001</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 43 columns</p>
</div>




```python
#List of desired information
actual_observed = ['Name','Type','Owner','Beds',
                   'C.diff Patient Days','C.diff Observed Cases',
                   'CAUTI: Number of Urinary Catheter Days', 'CAUTI: Observed Cases', 
                   'CLABSI: Number of Device Days','CLABSI: Observed Cases',
                   'MRSA Patient Days','MRSA Observed Cases',
                   'SSI: Colon, Number of Procedures','SSI: Colon Observed Cases','City','State','ZIP']
```


```python
#Dataframe of above
actual_observed_df = hosps_scores[actual_observed]
```


```python
#Replacing strings with int(0) for math stuff
#GPL 3.0/HT: Contributed/dragged out of me by Dennis Tran

for column in actual_observed_df.columns:
    actual_observed_df[column]=actual_observed_df[column].map(lambda x: 0 if x=='Not Available' else\
                                                              (0 if x==None else x))
```

    /home/spinster/anaconda3/envs/PythonData/lib/python3.6/site-packages/ipykernel_launcher.py:5: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: http://pandas.pydata.org/pandas-docs/stable/indexing.html#indexing-view-versus-copy
      """



```python
actual_observed_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Name</th>
      <th>Type</th>
      <th>Owner</th>
      <th>Beds</th>
      <th>C.diff Patient Days</th>
      <th>C.diff Observed Cases</th>
      <th>CAUTI: Number of Urinary Catheter Days</th>
      <th>CAUTI: Observed Cases</th>
      <th>CLABSI: Number of Device Days</th>
      <th>CLABSI: Observed Cases</th>
      <th>MRSA Patient Days</th>
      <th>MRSA Observed Cases</th>
      <th>SSI: Colon, Number of Procedures</th>
      <th>SSI: Colon Observed Cases</th>
      <th>City</th>
      <th>State</th>
      <th>ZIP</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>ABBEVILLE AREA MEDICAL CENTER</td>
      <td>GENERAL ACUTE CARE</td>
      <td>GOVERNMENT - LOCAL</td>
      <td>25.0</td>
      <td>3797</td>
      <td>0</td>
      <td>566</td>
      <td>0</td>
      <td>492</td>
      <td>0</td>
      <td>3797</td>
      <td>0</td>
      <td>11</td>
      <td>1</td>
      <td>ABBEVILLE</td>
      <td>SC</td>
      <td>29620</td>
    </tr>
    <tr>
      <th>1</th>
      <td>ABBEVILLE GENERAL HOSPITAL</td>
      <td>GENERAL ACUTE CARE</td>
      <td>REGIONAL GOVERNMENT</td>
      <td>60.0</td>
      <td>5392</td>
      <td>5</td>
      <td>2183</td>
      <td>1</td>
      <td>828</td>
      <td>0</td>
      <td>5470</td>
      <td>1</td>
      <td>17</td>
      <td>0</td>
      <td>ABBEVILLE</td>
      <td>LA</td>
      <td>70510</td>
    </tr>
    <tr>
      <th>2</th>
      <td>ABBOTT NORTHWESTERN HOSPITAL</td>
      <td>GENERAL ACUTE CARE</td>
      <td>NON-PROFIT</td>
      <td>952.0</td>
      <td>166574</td>
      <td>79</td>
      <td>19667</td>
      <td>26</td>
      <td>20190</td>
      <td>20</td>
      <td>171644</td>
      <td>1</td>
      <td>342</td>
      <td>11</td>
      <td>MINNEAPOLIS</td>
      <td>MN</td>
      <td>55407</td>
    </tr>
    <tr>
      <th>3</th>
      <td>ABILENE REGIONAL MEDICAL CENTER</td>
      <td>GENERAL ACUTE CARE</td>
      <td>PROPRIETARY</td>
      <td>231.0</td>
      <td>21895</td>
      <td>9</td>
      <td>3668</td>
      <td>1</td>
      <td>3371</td>
      <td>2</td>
      <td>26262</td>
      <td>1</td>
      <td>48</td>
      <td>0</td>
      <td>ABILENE</td>
      <td>TX</td>
      <td>79606</td>
    </tr>
    <tr>
      <th>4</th>
      <td>ABINGTON MEMORIAL HOSPITAL</td>
      <td>GENERAL ACUTE CARE</td>
      <td>NON-PROFIT</td>
      <td>665.0</td>
      <td>133135</td>
      <td>52</td>
      <td>15988</td>
      <td>32</td>
      <td>15411</td>
      <td>15</td>
      <td>146676</td>
      <td>9</td>
      <td>362</td>
      <td>11</td>
      <td>ABINGTON</td>
      <td>PA</td>
      <td>19001</td>
    </tr>
  </tbody>
</table>
</div>




```python
actual_observed_df = actual_observed_df.apply(pd.to_numeric, errors='ignore')
```


```python
len(actual_observed_df)
```




    3826




```python
#To be used for calculations & plots.
float_df = actual_observed_df.iloc[:,3:-3].astype(float)
```


```python
float_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Beds</th>
      <th>C.diff Patient Days</th>
      <th>C.diff Observed Cases</th>
      <th>CAUTI: Number of Urinary Catheter Days</th>
      <th>CAUTI: Observed Cases</th>
      <th>CLABSI: Number of Device Days</th>
      <th>CLABSI: Observed Cases</th>
      <th>MRSA Patient Days</th>
      <th>MRSA Observed Cases</th>
      <th>SSI: Colon, Number of Procedures</th>
      <th>SSI: Colon Observed Cases</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>25.0</td>
      <td>3797.0</td>
      <td>0.0</td>
      <td>566.0</td>
      <td>0.0</td>
      <td>492.0</td>
      <td>0.0</td>
      <td>3797.0</td>
      <td>0.0</td>
      <td>11.0</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>60.0</td>
      <td>5392.0</td>
      <td>5.0</td>
      <td>2183.0</td>
      <td>1.0</td>
      <td>828.0</td>
      <td>0.0</td>
      <td>5470.0</td>
      <td>1.0</td>
      <td>17.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>952.0</td>
      <td>166574.0</td>
      <td>79.0</td>
      <td>19667.0</td>
      <td>26.0</td>
      <td>20190.0</td>
      <td>20.0</td>
      <td>171644.0</td>
      <td>1.0</td>
      <td>342.0</td>
      <td>11.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>231.0</td>
      <td>21895.0</td>
      <td>9.0</td>
      <td>3668.0</td>
      <td>1.0</td>
      <td>3371.0</td>
      <td>2.0</td>
      <td>26262.0</td>
      <td>1.0</td>
      <td>48.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>665.0</td>
      <td>133135.0</td>
      <td>52.0</td>
      <td>15988.0</td>
      <td>32.0</td>
      <td>15411.0</td>
      <td>15.0</td>
      <td>146676.0</td>
      <td>9.0</td>
      <td>362.0</td>
      <td>11.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
#This is the last of the cleaning. Starting calculations and plots
```


```python
#Calculation
# Multiply that by number of observed cases to get prevalence per infection in each hospital
c_diff_per = (float_df['C.diff Observed Cases']/float_df['Beds'])*100
float_df['% C. diff'] = c_diff_per
uti_per = (float_df['CAUTI: Observed Cases']/float_df['Beds'])*100
float_df['% CAUTI'] = uti_per
bsi_per = (float_df['CLABSI: Observed Cases']/float_df['Beds'])*100
float_df['% CLABSI'] = bsi_per
mrsa_per = (float_df['MRSA Observed Cases']/float_df['Beds'])*100
float_df['% MRSA'] = mrsa_per
ssi_per = float_df['SSI: Colon, Number of Procedures']/float_df['Beds']
float_df['% SSI'] = ssi_per
```


```python
final_df = float_df[['Beds',
         'C.diff Patient Days','C.diff Observed Cases','% C. diff',
        'CAUTI: Number of Urinary Catheter Days','CAUTI: Observed Cases','% CAUTI',
        'CLABSI: Number of Device Days','CLABSI: Observed Cases','% CLABSI',
        'MRSA Patient Days','MRSA Observed Cases','% MRSA',
        'SSI: Colon, Number of Procedures','SSI: Colon Observed Cases','% SSI']]
```


```python
final_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Beds</th>
      <th>C.diff Patient Days</th>
      <th>C.diff Observed Cases</th>
      <th>% C. diff</th>
      <th>CAUTI: Number of Urinary Catheter Days</th>
      <th>CAUTI: Observed Cases</th>
      <th>% CAUTI</th>
      <th>CLABSI: Number of Device Days</th>
      <th>CLABSI: Observed Cases</th>
      <th>% CLABSI</th>
      <th>MRSA Patient Days</th>
      <th>MRSA Observed Cases</th>
      <th>% MRSA</th>
      <th>SSI: Colon, Number of Procedures</th>
      <th>SSI: Colon Observed Cases</th>
      <th>% SSI</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>25.0</td>
      <td>3797.0</td>
      <td>0.0</td>
      <td>0.000000</td>
      <td>566.0</td>
      <td>0.0</td>
      <td>0.000000</td>
      <td>492.0</td>
      <td>0.0</td>
      <td>0.000000</td>
      <td>3797.0</td>
      <td>0.0</td>
      <td>0.000000</td>
      <td>11.0</td>
      <td>1.0</td>
      <td>0.440000</td>
    </tr>
    <tr>
      <th>1</th>
      <td>60.0</td>
      <td>5392.0</td>
      <td>5.0</td>
      <td>8.333333</td>
      <td>2183.0</td>
      <td>1.0</td>
      <td>1.666667</td>
      <td>828.0</td>
      <td>0.0</td>
      <td>0.000000</td>
      <td>5470.0</td>
      <td>1.0</td>
      <td>1.666667</td>
      <td>17.0</td>
      <td>0.0</td>
      <td>0.283333</td>
    </tr>
    <tr>
      <th>2</th>
      <td>952.0</td>
      <td>166574.0</td>
      <td>79.0</td>
      <td>8.298319</td>
      <td>19667.0</td>
      <td>26.0</td>
      <td>2.731092</td>
      <td>20190.0</td>
      <td>20.0</td>
      <td>2.100840</td>
      <td>171644.0</td>
      <td>1.0</td>
      <td>0.105042</td>
      <td>342.0</td>
      <td>11.0</td>
      <td>0.359244</td>
    </tr>
    <tr>
      <th>3</th>
      <td>231.0</td>
      <td>21895.0</td>
      <td>9.0</td>
      <td>3.896104</td>
      <td>3668.0</td>
      <td>1.0</td>
      <td>0.432900</td>
      <td>3371.0</td>
      <td>2.0</td>
      <td>0.865801</td>
      <td>26262.0</td>
      <td>1.0</td>
      <td>0.432900</td>
      <td>48.0</td>
      <td>0.0</td>
      <td>0.207792</td>
    </tr>
    <tr>
      <th>4</th>
      <td>665.0</td>
      <td>133135.0</td>
      <td>52.0</td>
      <td>7.819549</td>
      <td>15988.0</td>
      <td>32.0</td>
      <td>4.812030</td>
      <td>15411.0</td>
      <td>15.0</td>
      <td>2.255639</td>
      <td>146676.0</td>
      <td>9.0</td>
      <td>1.353383</td>
      <td>362.0</td>
      <td>11.0</td>
      <td>0.544361</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Number of infections by bed

#Change fig size
plt.figure(figsize=(20,10))

plt.scatter(final_df['Beds'], final_df['C.diff Observed Cases'], c = 'red', marker='o', s=final_df['% C. diff'], alpha=0.3, label='C. diff')
plt.scatter(final_df['Beds'], final_df['CAUTI: Observed Cases'], c = 'yellow', marker='o', s=final_df['% CAUTI'], alpha=0.3, label='UTI')
plt.scatter(final_df['Beds'], final_df['CLABSI: Observed Cases'], c = 'blue', marker='o', s=final_df['% CLABSI'], alpha=0.3, label='BSI')
plt.scatter(final_df['Beds'], final_df['MRSA Observed Cases'], c = 'green', marker='o', s=final_df['% MRSA'], alpha=0.3, label='MRSA')
plt.scatter(final_df['Beds'], final_df['SSI: Colon Observed Cases'], c = 'purple', marker='o', s=final_df['% SSI'], alpha=0.3, label = 'SSI')

#adding grid            
#plt.grid(True)



# x-axis labels, y-axis labels, title, legend
plt.xlim(-5,1750)
plt.title('Observed Infections by Beds')
plt.xlabel('Beds')
plt.ylabel('Observed Infections')
plt.legend(loc='best')
plt.savefig('observed_infections_by_bed.png')
```


![png](output_33_0.png)



```python
#stacked bar chart of above scatter plot
plt.figure(figsize=(20,10))
plt.bar(final_df['Beds'], final_df['C.diff Observed Cases'], color = 'red', alpha=0.5, label='C. diff')
plt.bar(final_df['Beds'], final_df['CAUTI: Observed Cases'], color = 'yellow', alpha=0.5, label='UTI')
plt.bar(final_df['Beds'], final_df['CLABSI: Observed Cases'], color = 'blue', alpha=0.5, label='BSI')
plt.bar(final_df['Beds'], final_df['MRSA Observed Cases'], color = 'green', alpha=0.5, label='MRSA')
plt.bar(final_df['Beds'], final_df['SSI: Colon Observed Cases'], color = 'purple', alpha=0.5, label = 'SSI')


#adding grid            
#plt.grid(True)

plt.xlim(0,1000)
plt.ylim(0,275)
plt.title('Observed Infections by Beds')
plt.xlabel('Beds')
plt.ylabel('Observed Infections')
plt.legend(loc='best')
plt.savefig('observed_infections_by_bed-bar.png')
```


![png](output_34_0.png)



```python
#Hospital types
hosp_types = (actual_observed_df['Type']).values
hosp_types = np.unique(hosp_types)
```


```python
#Hospital owners
hosp_owner = (actual_observed_df['Owner']).values
hosp_owner = np.unique(hosp_owner)
```


```python
#Filter by owner 'GOVERNMENT - FEDERAL' 'GOVERNMENT - LOCAL' 'GOVERNMENT - STATE' 'NON-PROFIT' 'NOT AVAILABLE' 'PROPRIETARY' 'REGIONAL GOVERNMENT'
fed = actual_observed_df[actual_observed_df['Owner']=='GOVERNMENT - FEDERAL']
state = actual_observed_df[actual_observed_df['Owner']=='GOVERNMENT - STATE']
charity = actual_observed_df[actual_observed_df['Owner']=='NON-PROFIT']
#not_avail = actual_observed_df[actual_observed_df['Owner']=='NOT AVAILABLE']
private = actual_observed_df[actual_observed_df['Owner']=='PROPRIETARY']
regional = actual_observed_df[actual_observed_df['Owner']=='REGIONAL GOVERNMENT']
```


```python
#Filter by type 'CHILDREN' 'CHRONIC DISEASE' 'CRITICAL ACCESS' 'GENERAL ACUTE CARE' 'LONG TERM CARE' 'PSYCHIATRIC' 'REHABILITATION' 'SPECIAL' 'WOMEN'
child = actual_observed_df[actual_observed_df['Type']=='CHILDREN']
chronic = actual_observed_df[actual_observed_df['Type']=='CHRONIC DISEASE']
critical =actual_observed_df[actual_observed_df['Type']=='CRITICAL ACCESS']
general = actual_observed_df[actual_observed_df['Type']=='GENERAL ACUTE CARE']
LTC = actual_observed_df[actual_observed_df['Type']=='LONG TERM CARE']
psych =actual_observed_df[actual_observed_df['Type']=='PSYCHIATRIC']
rehab =actual_observed_df[actual_observed_df['Type']=='REHABILITATION']
special =actual_observed_df[actual_observed_df['Type']=='SPECIAL']
women = actual_observed_df[actual_observed_df['Type']=='WOMEN']
```


```python
#Mean by owner 
fed_mean = fed.groupby(['Owner']).mean()
state_mean = state.groupby(['Owner']).mean()
charity_mean = charity.groupby(['Owner']).mean()
#not_avail_bed = not_avail.groupby(['Owner','Beds']).mean()
private_mean = private.groupby(['Owner']).mean()
regional_mean = regional.groupby(['Owner']).mean()
```


```python
#Mean by type
child_mean = child.groupby(['Type']).mean()
chronic_mean = chronic.groupby(['Type']).mean()
critical_mean = critical.groupby(['Type']).mean()
general_mean = general.groupby(['Type']).mean()
LTC_mean = LTC.groupby(['Type']).mean()
psych_mean = psych.groupby(['Type']).mean()
rehab_mean = rehab.groupby(['Type']).mean()
special_mean = special.groupby(['Type']).mean()
women_mean = women.groupby(['Type']).mean()
```


```python
#beds by type
beds_types = [child_mean['Beds'].values[0],
              chronic_mean['Beds'].values[0],
              critical_mean['Beds'].values[0],
              general_mean['Beds'].values[0],
              LTC_mean['Beds'].values[0],
              psych_mean['Beds'].values[0],
              rehab_mean['Beds'].values[0],
              special_mean['Beds'].values[0],
              women_mean['Beds'].values[0]] 
```


```python
#bar graph average beds by type of hospital

fig, ax = plt.subplots(figsize=(10,5))
ind = np.arange(len(beds_types))
labels = ['Children','Chronic','Critical','General Acute','LTC','Psychiatric','Rehabilitation','Special','Women']

width = 0.75
ax.set_xticks(ind)
ax.set_title("Number of Beds by FacilityType")
ax.set_ylabel("Average Number of Beds")
ax.set_xticklabels(labels, rotation=45)
beds_type = ax.bar(ind,beds_types, width, color = "pink")

plt.grid(True)
plt.savefig('beds_by_type.png')
```


![png](output_42_0.png)



```python
#beds by owner of hospital
beds_owner = [fed_mean['Beds'].values[0],
            regional_mean['Beds'].values[0],
            state_mean['Beds'].values[0],
            charity_mean['Beds'].values[0],
            private_mean['Beds'].values[0]]
```


```python
#bar graph average beds by owner
fig, ax = plt.subplots(figsize=(10,5))
ind = np.arange(len(beds_owner))

ax.set_xticks(ind)
ax.set_title("Number of Beds by Owner")
ax.set_ylabel("Average Number of Beds")
ax.set_xticklabels(('Federal','Regional','State','Non-Profit','Private'))
width = 0.75
beds_owner = ax.bar(ind,beds_owner, width, color = "cyan")

plt.grid(True)
plt.savefig('beds_by_owner.png')
```


![png](output_44_0.png)



```python
#owner by type
fed_own = fed.groupby(['Type'])['Owner'].count()
state_own = state.groupby(['Type'])['Owner'].count()
charity_own = charity.groupby(['Type'])['Owner'].count()
private_own = private.groupby(['Type'])['Owner'].count()
regional_own = regional.groupby(['Type'])['Owner'].count()
```


```python
#Percentage of each type by owner
fed_calc = sum(fed_own)
fed_per = (fed_own/fed_calc)*100

state_calc = sum(state_own)
state_per = (state_own/state_calc)*100

charity_calc = sum(charity_own)
charity_per = (charity_own/charity_calc)*100

priv_calc = sum(private_own)
priv_per = (private_own/priv_calc)*100

reg_calc = sum(regional_own)
reg_per = (regional_own/reg_calc)*100
```


```python
#DF of percentage type by owner
owner_type_percent = {'Federal':fed_per,
                      'Regional':reg_per,
                      'State':state_per, 
                      'Non-Profit':charity_per, 
                      'Private':priv_per}
owner_type_percent_df = pd.DataFrame(owner_type_percent)
```


```python
owner_type_percent_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Federal</th>
      <th>Non-Profit</th>
      <th>Private</th>
      <th>Regional</th>
      <th>State</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CHILDREN</th>
      <td>NaN</td>
      <td>2.179177</td>
      <td>0.547945</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>CHRONIC DISEASE</th>
      <td>NaN</td>
      <td>0.048426</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>CRITICAL ACCESS</th>
      <td>57.142857</td>
      <td>20.387409</td>
      <td>4.931507</td>
      <td>41.036717</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>GENERAL ACUTE CARE</th>
      <td>42.857143</td>
      <td>76.658596</td>
      <td>87.808219</td>
      <td>58.747300</td>
      <td>85.0</td>
    </tr>
    <tr>
      <th>LONG TERM CARE</th>
      <td>NaN</td>
      <td>0.048426</td>
      <td>0.958904</td>
      <td>NaN</td>
      <td>2.5</td>
    </tr>
    <tr>
      <th>PSYCHIATRIC</th>
      <td>NaN</td>
      <td>0.242131</td>
      <td>0.958904</td>
      <td>0.215983</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>REHABILITATION</th>
      <td>NaN</td>
      <td>0.145278</td>
      <td>0.410959</td>
      <td>NaN</td>
      <td>2.5</td>
    </tr>
    <tr>
      <th>SPECIAL</th>
      <td>NaN</td>
      <td>0.242131</td>
      <td>4.246575</td>
      <td>NaN</td>
      <td>10.0</td>
    </tr>
    <tr>
      <th>WOMEN</th>
      <td>NaN</td>
      <td>0.048426</td>
      <td>0.136986</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>




```python
#DF looks better with 0 replacing NaN
owner_type_percent_df = owner_type_percent_df.fillna(0)
```


```python
owner_type_percent_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Federal</th>
      <th>Non-Profit</th>
      <th>Private</th>
      <th>Regional</th>
      <th>State</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>CHILDREN</th>
      <td>0.000000</td>
      <td>2.179177</td>
      <td>0.547945</td>
      <td>0.000000</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>CHRONIC DISEASE</th>
      <td>0.000000</td>
      <td>0.048426</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>CRITICAL ACCESS</th>
      <td>57.142857</td>
      <td>20.387409</td>
      <td>4.931507</td>
      <td>41.036717</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>GENERAL ACUTE CARE</th>
      <td>42.857143</td>
      <td>76.658596</td>
      <td>87.808219</td>
      <td>58.747300</td>
      <td>85.0</td>
    </tr>
    <tr>
      <th>LONG TERM CARE</th>
      <td>0.000000</td>
      <td>0.048426</td>
      <td>0.958904</td>
      <td>0.000000</td>
      <td>2.5</td>
    </tr>
    <tr>
      <th>PSYCHIATRIC</th>
      <td>0.000000</td>
      <td>0.242131</td>
      <td>0.958904</td>
      <td>0.215983</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>REHABILITATION</th>
      <td>0.000000</td>
      <td>0.145278</td>
      <td>0.410959</td>
      <td>0.000000</td>
      <td>2.5</td>
    </tr>
    <tr>
      <th>SPECIAL</th>
      <td>0.000000</td>
      <td>0.242131</td>
      <td>4.246575</td>
      <td>0.000000</td>
      <td>10.0</td>
    </tr>
    <tr>
      <th>WOMEN</th>
      <td>0.000000</td>
      <td>0.048426</td>
      <td>0.136986</td>
      <td>0.000000</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# C. diff by owner
cd_owner = [fed_mean['C.diff Observed Cases'].values[0], 
            regional_mean['C.diff Observed Cases'].values[0],
            state_mean['C.diff Observed Cases'].values[0],
            charity_mean['C.diff Observed Cases'].values[0],
            private_mean['C.diff Observed Cases'].values[0]]
```


```python
#bar graph of mean by owner, C. diff
fig, ax = plt.subplots(figsize=(10,5))
ind = np.arange(len(cd_owner))

ax.set_xticks(ind)
ax.set_title("Clostridium difficile Infections by Owner")
ax.set_ylabel("Average Number of Cases")
ax.set_xticklabels(('Federal','Regional','State','Non-Profit','Private'))
width = 0.75
cd = ax.bar(ind,cd_owner, width, color = 'red')

plt.grid(True)
plt.savefig('cd_by_owner.png')
```


![png](output_52_0.png)



```python
#mean of all observed infections by owner
all_infect_fed = (fed['C.diff Observed Cases'] +
              fed['CAUTI: Observed Cases'] +
              fed['CLABSI: Observed Cases'] +
              fed['MRSA Observed Cases'] +
              fed['SSI: Colon Observed Cases']).values[0]

all_infect_reg = (regional['C.diff Observed Cases'] +
              regional['CAUTI: Observed Cases'] +
              regional['CLABSI: Observed Cases'] +
              regional['MRSA Observed Cases'] +
              regional['SSI: Colon Observed Cases']).values[0]

all_infect_state = (state['C.diff Observed Cases'] +
              state['CAUTI: Observed Cases'] +
              state['CLABSI: Observed Cases'] +
              state['MRSA Observed Cases'] +
              state['SSI: Colon Observed Cases']).values[0]
all_infect_non = (charity['C.diff Observed Cases'] +
              charity['CAUTI: Observed Cases'] +
              charity['CLABSI: Observed Cases'] +
              charity['MRSA Observed Cases'] +
              charity['SSI: Colon Observed Cases']).values[0]

all_infect_priv = (private['C.diff Observed Cases'] +
              private['CAUTI: Observed Cases'] +
              private['CLABSI: Observed Cases'] +
              private['MRSA Observed Cases'] +
              private['SSI: Colon Observed Cases']).values[0]
```


```python
#List for graph of infections by owner
own_infect = [all_infect_fed, all_infect_reg, all_infect_state,all_infect_non,all_infect_priv]
```


```python
#Graph of average infections by owner
fig, ax = plt.subplots(figsize=(10,5))
ind = np.arange(len(own_infect))

ax.set_xticks(ind)
ax.set_title("Average of All Infections by Owner")
ax.set_ylabel('Average Infections')
ax.set_xticklabels(('Federal','Regional','State','Non-Profit','Private'))
width = 0.75
own = ax.bar(ind,own_infect, width, color = "orange")

plt.grid(True)
plt.savefig('avg_infect_by_owner.png')
```


![png](output_55_0.png)



```python
#mean of all observed infections by type
#'Children','Chronic','Critical','General Acute','LTC','Psychiatric','Rehabilitation','Special','Women'
all_infect_child = (child['C.diff Observed Cases'] +
              child['CAUTI: Observed Cases'] +
              child['CLABSI: Observed Cases'] +
              child['MRSA Observed Cases'] +
              child['SSI: Colon Observed Cases']).values[0]

all_infect_chronic = (chronic['C.diff Observed Cases'] +
              chronic['CAUTI: Observed Cases'] +
              chronic['CLABSI: Observed Cases'] +
              chronic['MRSA Observed Cases'] +
              chronic['SSI: Colon Observed Cases']).values[0]

all_infect_critical = (critical['C.diff Observed Cases'] +
              critical['CAUTI: Observed Cases'] +
              critical['CLABSI: Observed Cases'] +
              critical['MRSA Observed Cases'] +
              critical['SSI: Colon Observed Cases']).values[0]

all_infect_gen = (general['C.diff Observed Cases'] +
              general['CAUTI: Observed Cases'] +
              general['CLABSI: Observed Cases'] +
              general['MRSA Observed Cases'] +
              general['SSI: Colon Observed Cases']).values[0]

all_infect_ltc = (LTC['C.diff Observed Cases'] +
              LTC['CAUTI: Observed Cases'] +
              LTC['CLABSI: Observed Cases'] +
              LTC['MRSA Observed Cases'] +
              LTC['SSI: Colon Observed Cases']).values[0]

all_infect_psych = (psych['C.diff Observed Cases'] +
              psych['CAUTI: Observed Cases'] +
              psych['CLABSI: Observed Cases'] +
              psych['MRSA Observed Cases'] +
              psych['SSI: Colon Observed Cases']).values[0]

all_infect_rehab = (rehab['C.diff Observed Cases'] +
              rehab['CAUTI: Observed Cases'] +
              rehab['CLABSI: Observed Cases'] +
              rehab['MRSA Observed Cases'] +
              rehab['SSI: Colon Observed Cases']).values[0]

all_infect_special = (special['C.diff Observed Cases'] +
              special['CAUTI: Observed Cases'] +
              special['CLABSI: Observed Cases'] +
              special['MRSA Observed Cases'] +
              special['SSI: Colon Observed Cases']).values[0]

all_infect_women = (women['C.diff Observed Cases'] +
              women['CAUTI: Observed Cases'] +
              women['CLABSI: Observed Cases'] +
              women['MRSA Observed Cases'] +
              women['SSI: Colon Observed Cases']).values[0]

```


```python
#infections by facility
type_infect = [all_infect_child,
               all_infect_chronic,
               all_infect_critical,
               all_infect_gen,
               all_infect_ltc,
               all_infect_psych,
               all_infect_rehab,
               all_infect_special,
               all_infect_women]
```


```python
#average of all infections by facility type
#Interesting discovery about this missing data, therefore graph left out of presentation & explaination used instead.
fig, ax = plt.subplots(figsize=(10,5))
ind = np.arange(len(type_infect))
labels = ['Children','Chronic','Critical','General Acute','LTC','Psychiatric','Rehabilitation','Special','Women']

ax.set_xticks(ind)
ax.set_title("Average Infections by Facility Type")
ax.set_ylabel("Number of Infections")
ax.set_xticklabels(labels, rotation=45)
infect_type = ax.bar(ind,type_infect, width, color = "magenta")

plt.grid(True)
plt.savefig('average_infections_by_facility_type.png')
```


![png](output_58_0.png)



```python
#Setup for graph of C. diff by facility type
type_cd = [child_mean['C.diff Observed Cases'].values[0],
           chronic_mean['C.diff Observed Cases'].values[0],
           critical_mean['C.diff Observed Cases'].values[0],
           general_mean['C.diff Observed Cases'].values[0],
           LTC_mean['C.diff Observed Cases'].values[0],
           psych_mean['C.diff Observed Cases'].values[0],
           rehab_mean['C.diff Observed Cases'].values[0],
           special_mean['C.diff Observed Cases'].values[0],
           women_mean['C.diff Observed Cases'].values[0]]
```


```python
#Bar plot of C. diff by facility type
fig, ax = plt.subplots(figsize=(10,5))
ind = np.arange(len(type_cd))

ax.set_xticks(ind)
ax.set_title("Clostridium difficile Infections by Facility Type")
ax.set_ylabel("Average Number of Cases")
ax.set_xticklabels(('Children', 'Chronic, Critical', 'General', 'LTC','Psychiatric','Rehabilitation','Special','Women'))
ax.set_xticklabels(labels, rotation=45)
width = 0.75
bsi = ax.bar(ind,type_cd, width, color = 'red')

plt.grid(True)
plt.savefig('cd_by_type.png')
```


![png](output_60_0.png)

